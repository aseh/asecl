<%session("url")=request("url")%>
<html>

<head>
<title>Photo_registration</title>
</head>
<SCRIPT LANGUAGE=VBSCRIPT>
       Function PhotoListingForm_OnSubmit()
          if trim(Document.PhotoListingForm.Name.value) = "" then
             PhotoListingForm_OnSubmit = False
             Alert("Please input your name!!")
          else
             PhotoListingForm_OnSubmit = True
          end if    
          if trim(Document.PhotoListingForm.Email.value) = "" then
             PhotoListingForm_OnSubmit = False
             Alert("Please input your email!!")
          else
             PhotoListingForm_OnSubmit = Ture
          end if 
          if  trim(Document.PhotoListingForm.company.value) = "" then
             PhotoListingForm_OnSubmit = False
             Alert("Please input your company or Organization!!")
          else
             PhotoListingForm_OnSubmit = Ture
          end if 
          
          if instr(Document.PhotoListingForm.Email.value,"@") = 0 or instr(Document.PhotoListingForm.Email.value,".") = 0 then
             PhotoListingForm_OnSubmit = False
             Alert("Invalid mail address!!")
          else
             PhotoListingForm_OnSubmit = Ture
          end if 

          
       End Function
</SCRIPT>   

<body>
<table cellSpacing="0" cellPadding="0" width="700" border="0">
  <tbody>
    <tr>
      <td vAlign="center" align="middle" bgColor="#cccccc" colSpan="2" height="8"><img height="12" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="8"></td>
      <td width="644" bgColor="#cccccc" height="8"><img height="8" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="8"></td>
      <td width="40" bgColor="#cccccc" height="8"><img height="8" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="8"></td>
      <td width="1" bgColor="#000000" height="8"><img height="1" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="1"></td>
    </tr>
    <tr>
      <td vAlign="center" align="middle" bgColor="#000000" colSpan="2" height="50"><a href="http://www.ASE GROUP.com/english/default.htm"><img height="45" hspace="0" src="http://www.ASE GROUP.com/ASE GROUPdotcom/img/logo_40x33.gif" width="56" border="0"></a></td>
      <td class="whtext11" align="right" width="664" bgColor="#666666" height="50"><img height="10" src="http://www.ASE GROUP.com/chinese/img/spacer.gif" width="10"><a href="javascript:window.close()">Close   
        Window</a></td>
      <td width="40" bgColor="#666666" height="50"><img height="20" src="http://www.ASE GROUP.com/chinese/img/spacer.gif" width="40"></td>
      <td width="1" bgColor="#000000"><img height="1" src="http://www.ASE GROUP.com/chinese/img/spacer.gif" width="1"></td>
    </tr>
  </tbody>
</table>
<table cellSpacing="0" cellPadding="0" width="700" border="0">
  <tbody>
    <tr>
      <td width="15" bgColor="#ff0000"><img height="12" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="12"></td>
      <td width="40" bgColor="#ffffff"><img height="20" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="40"></td>
      <td class="gytext11" vAlign="top" align="left" width="605" bgColor="#ffffff"><br>
        <font class="rdheading">IMPORTANT - PLEASE READ THE FOLLOWING CAREFULLY 
        BEFORE CLICKING &quot;Accept&quot; BELOW:</font><br>
        <br>
        By clicking the &quot;Accept&quot; button below, you (either an   
        individual or entity) acknowledge that you have read and understood the   
        following terms and conditions to use photographs and other files on   
        this Photo Gallery and expressly agree to be bound by the following   
        terms and conditions.<strong> <u><font color="#003489">If you do not   
        agree to the following terms and conditions, please click 
        &quot;Decline&quot; and do not download any photographs and files on 
        this Photo Gallery.</font></u></strong><br>
        <br>
        I acknowledge and agree:<br>  
        <br>
        <table>
          <tbody>
            <tr>
              <td class="gytext11" vAlign="top">1.</td>
              <td class="gytext11">Subject to the terms and conditions hereof   
                and the provisions of the Legal   
                Notice and Trademark Information  on this Site, ASEGROUP
                (&quot;ASE GROUP&quot;) hereby grants me a   
                limited, nonexclusive, nonsublicensable right to use photographs   
                and other files on this Photo Gallery for lawful purposes only   
                and that any other use (e.g. editing, modification, etc.) must   
                request and receive prior written permission from ASE GROUP.<br>  
                <br>
              </td>
            </tr>
            <tr>
              <td class="gytext11" vAlign="top">2.</td>
              <td class="gytext11"><strong><u><font color="#17478f">Any use of 
                photographs and other files on this Photo Gallery must cite that 
                the source thereof is from &quot;ASEGROUP.&quot;.</font></u></strong><br>
                <br>
              </td>
            </tr>
            <tr>
              <td class="gytext11" vAlign="top">3.</td>
              <td class="gytext11">ASE GROUP, at its sole discretion, may restrict my 
                access to this Photo Gallery at any time without notice and/or 
                immediately terminate my use of photographs and other files 
                thereon upon providing notice, for my breach of the terms and 
                conditions hereof, including, without limitation, that ASE GROUP 
                believes that I have violated or acted inconsistently with the 
                terms and conditions hereof. <strong><u><font color="#17478f">Upon 
                notice from ASE GROUP that the use of photographs and other files I 
                downloaded on this Photo Gallery has been terminated, I must 
                immediately stop the use of such photographs and files and 
                destroy all copies thereof.</font></u></strong><br>
                <br>
              </td>
            </tr>
            <tr>
              <td class="gytext11" vAlign="top">4.</td>
              <td class="gytext11">Any breaching use of photographs and other 
                files on this Photo Gallery would result in irreparable injury 
                to ASE GROUP for which money damages would be inadequate and in such 
                event ASE GROUP shall have the right, in addition to other remedies 
                available at law and in equity, to immediate injunctive relief 
                to prevent any such breaching use. Nothing contained in this 
                provision or elsewhere herein shall be construed to limit 
                remedies or relief available pursuant to statutory or other 
                claims that ASE GROUP may have under separate legal authority.<br>
                <br>
              </td>
            </tr>
            <form name="PhotoListingForm" action="p_registration.asp" method="post">
              <tr>
                <td class="gytext11" vAlign="top">&nbsp;</td>
                <td><!--Dawn modified 2004/1/30-->
                  <table cellSpacing="1" cellPadding="1" width="100%" border="0">
                    <input type="hidden" value="false" name="hasInfo">
                    <tbody>
                      <tr>
                        <td class="rdheading" colSpan="3">Please enter your   
                          name, email address and company :</td>  
                      </tr>
                      <tr>
                        <td width="3%">&nbsp;</td>
                        <td class="gytext11" width="44%"><b>Name </b><input name="name" size="20"></td>  
                        <td class="gytext11" width="53%"><b>Email </b><input size="25" name="email"></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td class="gytext11" colSpan="2"><b>Company/Organization   
                          </b><input size="25" name="company"></td>
                      </tr>
                    </tbody>
                  </table>
                  <!--End Dawn modified 2004/2/2-->
                  <table cellSpacing="1" cellPadding="1" width="100%" border="0">
                    <tbody>
                      <tr>
                        <td colSpan="4"><font class="rdheading">Please select 
                          your purpose of using this picture :</font></td>
                      </tr>
                      <tr>
                        <td width="3%">&nbsp;</td>
                        <td  width="22%"><input  type="radio" name="purpose" value="Media coverage" checked> 
                          Media coverage</td>  
                        <td class="gytext11" width="73%">&nbsp;</td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td ><input type="radio" name="purpose" value="Personal use"> 
                          Personal use</td>  
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td ><input  type="radio" name="purpose" value="Research"> 
                          Research</td>  
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td  vAlign="top" colSpan="2"><input type="radio"  name="purpose" value="Others"> 
                          Others -- please specify : (200 characters limited)</td>  
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><textarea name="reason" rows="4" cols="50"></textarea></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="middle" colSpan="4"><input type="hidden" value="66" name="dataid"><input type="hidden" value="policyAgree" name="acAgree"><br>
                          <input onclick="return check()" type="submit" value="Accept" name="Submit"> 
                          <input onclick="window.close();" type="button" value="Decline" name="Cancel"></td>
                      </tr>
                    </tbody>
                  </table>
                  <!--End Dawn modified 2003/12/17-->
                </td>
              </tr>
            </form>
          </tbody>
        </table>
        <br>
        <br>
        <br>
      </td>
      <td width="40" bgColor="#ffffff"><img height="20" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="40"></td>
      <td width="1" bgColor="#000000"><img height="1" src="http://www.ASE GROUP.com/english/img/spacer.gif" width="1"></td>
    </tr>
  </tbody>
</table>
</body>

</html>
