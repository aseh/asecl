<%
'--------------------------------------------------------------------
' Microsoft CDONTS.NewMail
'
' (c) 1999 Microsoft Corporation.  All Rights Reserved.
'
' More NT Option Pack Documentation:
'   http://<ServerName>/iishelp/iis/htm/asp/amsm0qzj.htm
'
' CDONTS.NewMail constants include file for VBScript
'
'--------------------------------------------------------------------

'---- BodyFormat Property ----
Const CdoBodyFormatHTML	= 0	' The Body property is to include Hypertext Markup Language (HTML).
Const CdoBodyFormatText	= 1	' The Body property is to be exclusively in plain text (default value).

'---- MailFormat Property ----
Const CdoMailFormatMime	= 0	' The NewMail object is to be in MIME format.
Const CdoMailFormatText	= 1	' The NewMail object is to be in uninterrupted plain text (default value).

'---- Importance Property ----
Const CdoLow	= 0		' Low importance
Const CdoNormal	= 1		' Normal importance (default)
Const CdoHigh	= 2		' High importance

'---- AttachFile and AttachURL Methods ----
Const CdoEncodingUUencode	= 0	' The attachment is to be in UUEncode format (default).
Const CdoEncodingBase64		= 1	' The attachment is to be in base 64 format.
%>
