<HTML>
<HEAD><TITLE>Product Report</TITLE>
<%
	Set Connc = Server.CreateObject("ADODB.Connection")
	Connc.Open "DSN=webasecl;UID=cust_pdba;PWD=m2sbniob"
	
	'---------------------------------------------
	'	Update One Single Lot
	'---------------------------------------------
	If Request.Form("ayupdate")="Update" Then		
		sqlUpdate="UPDATE CS_MET_YIELD set yld_update=sysdate," &_
			" dice_in=" & Request.Form("dice_in") & " ,dice_out=" & Request.Form("dice_out") & " ,dice_rate=" &_
			"round(" & Request.Form("dice_out") & "/" & Request.Form("dice_in") & "*100,2)," &_
			
			" die_in=" & Request.Form("die_in") & " ,die_out=" & Request.Form("die_out") & " ,die_yld=" &_
			"round(" & Request.Form("die_out") & "/" & Request.Form("die_in") & "*100,2)," &_
			
			" pre_in=" & Request.Form("pre_in") & " ,pre_out=" & Request.Form("pre_out") & " ,pre_yld=" &_
			"round(" & Request.Form("pre_out") & "/" & Request.Form("pre_in") & "*100,2)," &_
			
			" ic_in=" & Request.Form("ic_in") & " ,ic_out=" & Request.Form("ic_out") & " ,ic_yld=" &_
			"round(" & Request.Form("ic_out") & "/" & Request.Form("ic_in") & "*100,2)," &_
			
			" yld_inv=" & "round(" & Request.Form("ic_out") & "/" & Request.Form("dice_in") & "*100,2)," &_
			" yld_act=" & "round(" & Request.Form("ic_out") & "/" & Request.Form("die_in") & "*100,2)," &_
			" yld_asy=" & "round(" & Request.Form("ic_out") & "/" & Request.Form("pre_in") & "*100,2)," &_
			" ship_qty=" & Request.Form("ship_qty") &_
			" WHERE lotno='" & Request.Form("id") & "' AND invoice='" & Request.Form("inv")&"' and  waferlot='" & Request.Form("waf") & "'"
		'response.write 	sqlUpdate
		Set rsUpdate=Connc.Execute(sqlUpdate)
		Response.Write "<BR><B>Update the record (INVOICE=" & Request.Form("inv") & " LOTNO=" & Request.Form("id") & ") completed.</B>"
		Response.Write "<HR><A HREF='AYreport.asp'>Back to Assembly Yield Report</A>"
		Response.End			
	End If
	
	'---------------------------------------------
	'	Delete One Single Lot
	'---------------------------------------------
	If Request.QueryString("name")="delete" Then
		sqlDel = "DELETE FROM CS_MET_YIELD WHERE lotno='" & Request.QueryString("id") & "' AND invoice='" & Request.QueryString("inv") & "' and  week=to_char(sysdate,'ww')"
		Set rsDel=Connc.Execute(sqlDel)
		Response.Write "<BR><B>Delete the record (INVOICE=" & Request.QueryString("inv") & " LOTNO=" & Request.QueryString("id") & ") completed.</B>"
		Response.Write "<HR><A HREF='AYreport.asp'>Back to Assembly Yield Report</A>"
		Response.End
	End If
	
	'---------------------------------------------
	'	Single Lot Query Listing
	'---------------------------------------------
	sqlAYc = "SELECT waferlot,invoice,device,lotno,pkg,pkg_size,l_c,dice_in,dice_out,dice_rate,die_in,die_out,die_yld,"&_
						"pre_in,pre_out,pre_yld,ic_in,ic_out,ic_yld,yld_inv,yld_act,yld_asy,ship_date,ship_qty,rowid "&_
  						"FROM CS_MET_YIELD WHERE lotno='" & Request.QueryString("id") & "' AND invoice='" & Request.QueryString("inv") & "' and waferlot='" & Request.QueryString("waferlot") & "'"
	  						
	Set rsAYc=Connc.Execute(sqlAYc)
%>

<SCRIPT LANGUAGE="JavaScript">
function diceCal() {
    dice_rate.innerText=(document.ay_change.dice_out.value/document.ay_change.dice_in.value)*100;
}

function dieCal() {
    die_yld.innerText=(document.ay_change.die_out.value/document.ay_change.die_in.value)*100;
}

function preCal() {
    pre_yld.innerText=(document.ay_change.pre_out.value/document.ay_change.pre_in.value)*100;
}

function icCal() {
    ic_yld.innerText=(document.ay_change.ic_out.value/document.ay_change.ic_in.value)*100;
    yield();
}

function yield() {
    yld_inv.innerText=(document.ay_change.ic_out.value/document.ay_change.dice_in.value)*100;
    yld_act.innerText=(document.ay_change.ic_out.value/document.ay_change.die_in.value)*100;
    yld_asy.innerText=(document.ay_change.ic_out.value/document.ay_change.pre_in.value)*100;
}

function back() {
    history.back();
}
</SCRIPT>

</HEAD><BODY>

<LINK href="global.css" type=text/css rel=stylesheet>
<a name="report_top"></a>
<table width="100%" border="0">                       
  <tr align="middle">
    <td><strong><font color="Teal">(ASECL)***ASSEMBLY YIELD REPORT-BASED ON INVOICE 
      DATE*** DATE: <%=FormatDateTime(now,vbShortDate)%></font></strong></td>
  </tr>
</table><BR>

<table width="99%" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="25%" align="left" class=footer></td>
    <td width="25%" align="left" class=footer></td>
  </tr>
</table>

<FORM name='ay_change' method='POST' action='AY_Change.asp'>
<TABLE width="100%">
  <TR> 
    <TD vAlign=top> 
      <TABLE class=lineColour cellSpacing=0 cellPadding=0 border=0>
        <TR bgcolor="#FFFFFF">
          <TD bgColor=#c0c0c0> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" align=left border=0>
              <TR> 
                <TD Align=center noWrap bgcolor="#009933" colspan="2"> <b><font color="#FFFFFF">Customer: ALCATEL
                  </font></b></TD>
              </TR>
              <TR bgcolor="#B5E3E6"> 
                <TD Align=center noWrap colspan="6">&nbsp;</TD>
                <TD Align=center noWrap colspan="3">DICE-DISCRP</TD>
                <TD Align=center noWrap colspan="3">DIE-INSPECT</TD>
                <TD Align=center noWrap colspan="3">PRE-MOLD</TD>
                <TD Align=center noWrap colspan="3">IC FINISHING</TD>
                <TD Align=center noWrap width='2%' colspan="3">YIELD</TD>
                <TD Align=center noWrap width='2%' colspan="2">SHIPPED</TD>
              </TR>
              <TR bgcolor="#B5E3E6"> 
                <TD Align=center noWrap width='7%' bgcolor="#FFFFFF">INVOICE</TD>
                <TD Align=center noWrap width='10%'>DEVICE TYPE</TD>
                <TD Align=center noWrap width='6%' bgcolor="#FFFFFF">LOT-NO</TD>
                <TD Align=center noWrap width='4%'>PKG</TD>
                <TD Align=center noWrap width='7%'>PKG SIZE</TD>
                <TD Align=center noWrap width='3%'>LC</TD>
                <TD Align=center noWrap width='2%' bgcolor="#FFFFFF">IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='9%'>RATE</TD>
                <TD Align=center noWrap width='2%' bgcolor="#FFFFFF">IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='9%'>YLD</TD>
                <TD Align=center noWrap width='2%' bgcolor="#FFFFFF">IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='8%'>YLD</TD>
                <TD Align=center noWrap width='3%'>IN</TD>
                <TD Align=center noWrap width='4%' bgcolor="#FFFFFF">OUT</TD>
                <TD Align=center noWrap width='8%'>YLD</TD>
                <TD Align=center noWrap width='0%'>INV</TD>
                <TD Align=center noWrap width='1%'>ACT</TD>
                <TD Align=center noWrap width='1%'>ASY</TD>
                <TD Align=center noWrap width='1%'>DATE</TD>
                <TD Align=center noWrap width='1%'>QTY</TD>
              </TR>
              <TR bgcolor="#B5E3E6"> 
                <TD Align=center noWrap width='7%'><%=rsAYc.fields("invoice").value%></TD>
                <TD Align=center noWrap width='10%'><%=rsAYc.fields("device").value%></TD>
                <TD Align=center noWrap width='6%'><%=rsAYc.fields("lotno").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAYc.fields("pkg").value%></TD>
                <TD Align=center noWrap width='7%'><%=rsAYc.fields("pkg_size").value%></TD>
                <TD Align=center noWrap width='3%'><%=rsAYc.fields("l_c").value%></TD>
                <TD Align=center noWrap width='2%'>
                <!------- Dice-DISCRP ------->                
                  <input type='text' name='dice_in' value='<%=rsAYc.fields("dice_in").value%>' 
                  onchange="diceCal()"
                  size='<%=Len(rsAYc.fields("dice_in").value)%>'>
                </TD>
                <TD Align=center noWrap width='4%'>
                  <input type='text' name='dice_out' value='<%=rsAYc.fields("dice_out").value%>'
                  onchange="diceCal()"
                  size='<%=Len(rsAYc.fields("dice_out").value)%>'>
                </TD>
                <TD Align=center noWrap width='9%' id='dice_rate'><%=rsAYc.fields("dice_rate").value%></TD>
                <!------- Die-INSPECT ------->
                <TD Align=center noWrap width='2%'>
                  <input type='text' name='die_in' value='<%=rsAYc.fields("die_in").value%>' 
                  onchange="dieCal()" 
                  size='<%=Len(rsAYc.fields("die_in").value)%>'>
                </TD>
                <TD Align=center noWrap width='4%'>
                  <input type='text' name='die_out' value='<%=rsAYc.fields("die_out").value%>' 
                  onchange="dieCal()" 
                  size='<%=Len(rsAYc.fields("die_out").value)%>'>
                </TD>
                <TD Align=center noWrap width='9%' id='die_yld'><%=rsAYc.fields("die_yld").value%></TD>
                <!------- PRE-MOLD ------->
                <TD Align=center noWrap width='2%'>
                  <input type='text' name='pre_in' value='<%=rsAYc.fields("pre_in").value%>' 
                  onchange="preCal()" 
                  size='<%=Len(rsAYc.fields("pre_in").value)%>'>
                </TD>
                <TD Align=center noWrap width='4%'>
                  <input type='text' name='pre_out' value='<%=rsAYc.fields("pre_out").value%>' 
                  onchange="preCal()" 
                  size='<%=Len(rsAYc.fields("pre_out").value)%>'>
                </TD>
                <TD Align=center noWrap width='8%' id='pre_yld'><%=rsAYc.fields("pre_yld").value%></TD>
                <!------- IC FINISHING ------->
                <TD Align=center noWrap width='3%'>
                  <input type='text' name='ic_in' value='<%=rsAYc.fields("ic_in").value%>' 
                  onchange="icCal()" 
                  size='<%=Len(rsAYc.fields("ic_in").value)%>'>
                </TD>
                <TD Align=center noWrap width='4%'>
                  <input type='text' name='ic_out' value='<%=rsAYc.fields("ic_out").value%>' 
                  onchange="icCal()" 
                  size='<%=Len(rsAYc.fields("ic_out").value)%>'>
                </TD>
                <TD Align=center noWrap width='8%' id='ic_yld'><%=rsAYc.fields("ic_yld").value%></TD>
                <!------- YIELD ------->
                <TD Align=center noWrap width='0%' id='yld_inv'><%=rsAYc.fields("yld_inv").value%></TD>
                <TD Align=center noWrap width='1%' id='yld_act'><%=rsAYc.fields("yld_act").value%></TD>
                <TD Align=center noWrap width='1%' id='yld_asy'><%=rsAYc.fields("yld_asy").value%></TD>
                <!------- SHIPPED ------->
                <TD Align=center noWrap width='1%'><%=rsAYc.fields("ship_date").value%></TD>
                <!--<TD Align=center noWrap width='1%'><%=rsAYc.fields("ship_qty").value%></TD>-->
                <TD Align=center noWrap width='4%'>
                  <input type='text' name='ship_qty' value='<%=rsAYc.fields("ship_qty").value%>'                   
                  size='<%=Len(rsAYc.fields("ship_qty").value)%>'>
                </TD>
                
              </TR>
              <TR bgcolor="#FFFF99"> 
                <TD Align=center noWrap width='7%'><b>
                <A HREF='AY_Change.asp?id=<% Response.Write Request.QueryString("id")& "&inv=" & Request.QueryString("inv")& "&name=delete" %>'>Delete</A></b></TD>
                <INPUT TYPE='hidden' name='id' value='<%=Request.QueryString("id")%>'>
                <INPUT TYPE='hidden' name='inv' value='<%=Request.QueryString("inv")%>'>
                <INPUT TYPE='hidden' name='waf' value='<%=Request.QueryString("waferlot")%>'>
                <TD Align=center noWrap width='10%'><input type='submit' name='ayupdate' value='Update'></TD>
                <TD Align=center noWrap width='6%'><input type='button' value='Cancel' onclick="back()"></TD>
                <TD Align=center noWrap width='4%' colspan='3'>Formula:</TD>
                <TD Align=center noWrap width='2%'>B</TD>
                <TD Align=center noWrap width='4%'>b</TD>
                <TD Align=center noWrap width='9%' bgcolor="#FFFFFF">b/B</TD>
                <TD Align=center noWrap width='2%'>C</TD>
                <TD Align=center noWrap width='4%'>c</TD>
                <TD Align=center noWrap width='9%' bgcolor="#FFFFFF">c/C</TD>
                <TD Align=center noWrap width='2%'>D</TD>
                <TD Align=center noWrap width='4%'>d</TD>
                <TD Align=center noWrap width='8%' bgcolor="#FFFFFF">d/D</TD>
                <TD Align=center noWrap width='3%'>A</TD>
                <TD Align=center noWrap width='4%'>a</TD>
                <TD Align=center noWrap width='8%'>a/A</TD>
                <TD Align=center noWrap width='0%' bgcolor="#FFFFFF">a/B</TD>
                <TD Align=center noWrap width='1%' bgcolor="#FFFFFF">a/C</TD>
                <TD Align=center noWrap width='1%' bgcolor="#FFFFFF">a/D</TD>
                <TD Align=center noWrap width='1%'>&nbsp;</TD>
                <TD Align=center noWrap width='1%'>&nbsp;</TD>
              </TR>

            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
    <TD vAlign=top></TD>
  </TR>
</TABLE>
</FORM>


<%	
	Set rsAYc = Nothing
	Connc.Close
%>

<TABLE width="100%" border="0">                       
  <TR align="right" class=footer><TD>[ <A HREF='AYreport.asp'>Back</A> ]</TD></TR>
</TABLE>
</BODY>
</HTML>