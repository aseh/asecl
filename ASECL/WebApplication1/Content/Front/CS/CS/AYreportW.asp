<HTML>
<HEAD><TITLE>Assembly Yield Report</TITLE>
<%
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open "DSN=webasecl;UID=cust_pdev;PWD=ufbrtv" 
	
	If Request.Form("select_week")="" Then
		sqlLC = "SELECT distinct l_c FROM CS_MET_YIELD where  week=to_char(sysdate,'ww')"
	Else
		sqlLC = "SELECT distinct l_c FROM CS_MET_YIELD where  week='" & Request.Form("select_week") & "'"
	End If
	
	Set rsLC=conn.Execute(sqlLC)
	sqlww="select to_char(sysdate,'ww')  ww from dual"
	Set rsww=conn.Execute(sqlWW)
	
	sqlweek = "select distinct week from cs_met_yield where year='" & year(now) & "'"
	Set rsweek=conn.Execute(sqlweek)
	
%>
</HEAD><BODY>
<LINK href="global.css" type=text/css rel=stylesheet>
<a name="report_top"></a>
<table width="100%" border="0">                       
  <tr align="middle">
    <td><strong><font color="Teal">(ASECL)***ASSEMBLY YIELD REPORT-BASED ON INVOICE 
      DATE*** DATE: <%=FormatDateTime(now,vbShortDate)%></font></strong></td>
  </tr>
</table><BR>

<table width="99%" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="25%" align="left" class=footer>FOR Week : WW<!--<%=rsww.fields("ww")%> -->
    <%
    	If Request.Form("select_week")="" Then
			Response.Write rsww.fields("ww")
			session("ww")=rsww.fields("ww")
		Else
			Response.Write Request.Form("select_week")
			session("ww") = Request.Form("select_week")
    	End If
    %><Form name=viewbyweek Action=AYreportW.asp Method=post>
      <select name="select_week">
        <%
        While Not rsweek.EOF
        	Response.Write "<option value='" & rsweek.fields("week").value & "'>" & rsweek.fields("week").value & "</option>"
        	rsweek.MoveNext
        Wend
        %>
      </select>
      <input type="submit" name="Submit" value="View by week">
      </Form>
      
      </td>
      
    <td width="25%" align="left" class=footer>
      <div align="right">[ <a href="#" onClick="MTD = window.open('mtd.asp','MTD','width=260,height=460,scrollbars=yes');return false;" target=_blank>MTD Related Information</a> ] <BR> [ 
        <A HREF="/cs/cs/fawebnew/excel/AY_Rpt.xls" target=_blank>Download as Excel</A> 
        ]</div>
    </td>
  </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0"><TR> 
<TD Align=left noWrap> <b>Customer: AMIS</b></TD>
</TR></table>

<!-- Beginning of LC Type -->
<% Do While Not rsLC.EOF %>
<TABLE width="100%">
  <TR> 
    <TD vAlign=top> 
      <TABLE class=lineColour cellSpacing=0 cellPadding=0 border=0>
        <TR bgcolor="#FFFFFF">
          <TD bgColor=#c0c0c0> 
            <TABLE cellSpacing=1 cellPadding=4 width="100%" align=left border=0>    
              <TR bgcolor="#B5E3E6"><!-- Major Columns -->
              	<% 
              		If Request.Form("select_week")="" Then
              			sqlAY = "SELECT waferlot,invoice,device,lotno,pkg,pkg_size,l_c,dice_in,dice_out,dice_rate,die_in,die_out,die_yld,"&_
						"pre_in,pre_out,pre_yld,ic_in,ic_out,ic_yld,yld_inv,yld_act,yld_asy,ship_date,ship_qty,rowid "&_
  						"FROM CS_MET_YIELD WHERE l_c='" & rsLC.fields("l_c").value & "' and  week=to_char(sysdate,'ww')"
  					Else
  						sqlAY = "SELECT waferlot,invoice,device,lotno,pkg,pkg_size,l_c,dice_in,dice_out,dice_rate,die_in,die_out,die_yld,"&_
						"pre_in,pre_out,pre_yld,ic_in,ic_out,ic_yld,yld_inv,yld_act,yld_asy,ship_date,ship_qty,rowid "&_
  						"FROM CS_MET_YIELD WHERE l_c='" & rsLC.fields("l_c").value & "' and  week='" & Request.Form("select_week") & "'"
  					End If
  					
					Set rsAY=conn.Execute(sqlAY)
				%>
                <TD Align=center noWrap colspan="6">&nbsp;</TD>
                <TD Align=center noWrap colspan="3">DICE-DISCRP</TD>
                <TD Align=center noWrap colspan="3">DIE-INSPECT</TD>
                <TD Align=center noWrap colspan="3">PRE-MOLD</TD>
                <TD Align=center noWrap colspan="3">IC FINISHING</TD>
                <TD Align=center noWrap width='2%' colspan="3">YIELD</TD>
                <TD Align=center noWrap width='2%' colspan="2">SHIPPED</TD>
              </TR>
              <TR bgcolor="#B5E3E6"><!-- Minor Columns -->
                <TD Align=center noWrap width='7%'>INVOICE</TD>
                <TD Align=center noWrap width='10%'>DEVICE TYPE</TD>
                <TD Align=center noWrap width='6%'>LOT-NO</TD>
                <TD Align=center noWrap width='4%'>PKG</TD>
                <TD Align=center noWrap width='7%'>PKG SIZE</TD>
                <TD Align=center noWrap width='3%'>LC</TD>
                <TD Align=center noWrap width='2%'>IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='9%'>RATE</TD>
                <TD Align=center noWrap width='2%'>IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='9%'>YLD</TD>
                <TD Align=center noWrap width='2%'>IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='8%'>YLD</TD>
                <TD Align=center noWrap width='3%'>IN</TD>
                <TD Align=center noWrap width='4%'>OUT</TD>
                <TD Align=center noWrap width='8%'>YLD</TD>
                <TD Align=center noWrap width='0%'>INV</TD>
                <TD Align=center noWrap width='1%'>ACT</TD>
                <TD Align=center noWrap width='1%'>ASY</TD>
                <TD Align=center noWrap width='1%'>DATE</TD>
                <TD Align=center noWrap width='1%'>QTY</TD>
              </TR>
              <%
              	Do While Not rsAY.EOF
              %>
              <TR bgcolor="#FFFFFF">
                <TD Align=left noWrap width='7%'>
                	<!--<A Href='AY_Change.asp?id=<%=rsAY.fields("lotno") & "&inv=" & rsAY.fields("invoice").value & "&waferlot=" & rsAY.fields("waferlot").value & "'>"%>
                	</A>-->
                	&nbsp;<%=rsAY.fields("invoice").value%></TD>
                <TD Align=center noWrap width='10%'><%=rsAY.fields("device").value%></TD>
                <TD Align=center noWrap width='6%'><%=rsAY.fields("lotno").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAY.fields("pkg").value%></TD>
                <TD Align=center noWrap width='7%'><%=rsAY.fields("pkg_size").value%></TD>
                <TD Align=center noWrap width='3%'><%=rsAY.fields("l_c").value%></TD>
                <TD Align=center noWrap width='2%'><%=rsAY.fields("dice_in").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAY.fields("dice_out").value%></TD>
                <%
                	'If CDbl(rsAY.fields("dice_rate").value) > 100.00 or CDbl(rsAY.fields("dice_rate").value) < 99.5 Then
                		'Response.Write "<TD Align=center noWrap width='9%'>" & _
                		'"<A href='AY_Change.asp?id=" & rsAY.fields("lotno") & "&inv=" & rsAY.fields("invoice").value & "&waferlot=" & rsAY.fields("waferlot").value & "'><font color=red>" & rsAY.fields("dice_rate").value & "</font></A></TD>"
                	'Else
                		Response.Write "<TD Align=center noWrap width='9%'>" & rsAY.fields("dice_rate").value & "</TD>"
                	'End If
                %>
                <!--<TD Align=center noWrap width='9%'><%=rsAY.fields("dice_rate").value%></TD>-->
                <TD Align=center noWrap width='2%'><%=rsAY.fields("die_in").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAY.fields("die_out").value%></TD>
                <%
                	'If CDbl(rsAY.fields("die_yld").value) > 100.00 Then
                		'Response.Write "<TD Align=center noWrap width='9%'>" & _
                		'"<A href='AY_Change.asp?id=" & rsAY.fields("lotno") & "&inv=" & rsAY.fields("invoice").value & "&waferlot=" & rsAY.fields("waferlot").value & "'><font color=red>" & rsAY.fields("die_yld").value & "</font></A></TD>"
                	'Else
                		Response.Write "<TD Align=center noWrap width='9%'>" & rsAY.fields("die_yld").value & "</TD>"
                	'End If
                %>
                
                <!--<TD Align=center noWrap width='9%'><%=rsAY.fields("die_yld").value%></TD>-->
                <TD Align=center noWrap width='2%'><%=rsAY.fields("pre_in").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAY.fields("pre_out").value%></TD>
                <%
                	If Len(rsAY.fields("pre_yld").value) = 6 Then
                		Response.Write "<TD Align=center noWrap width='8%'>" & _
                		"<A href='AY_Change.asp?id=" & rsAY.fields("lotno") & "&inv=" & rsAY.fields("invoice").value & "&waferlot=" & rsAY.fields("waferlot").value & "'><font color=red>" & rsAY.fields("pre_yld").value & "</font></A></TD>"
                	Else
                		Response.Write "<TD Align=center noWrap width='8%'>" & rsAY.fields("pre_yld").value & "</TD>"
                	End If
                %>
                
                <!--<TD Align=center noWrap width='8%'><%=rsAY.fields("pre_yld").value%></TD>-->
                <TD Align=center noWrap width='3%'><%=rsAY.fields("ic_in").value%></TD>
                <TD Align=center noWrap width='4%'><%=rsAY.fields("ic_out").value%></TD>
                <%
                	If Len(rsAY.fields("ic_yld").value) = 6 Then
                		Response.Write "<TD Align=center noWrap width='8%'>" & _
                		"<A href='AY_Change.asp?id=" & rsAY.fields("lotno") & "&inv=" & rsAY.fields("invoice").value & "&waferlot=" & rsAY.fields("waferlot").value & "'><font color=red>" & rsAY.fields("ic_yld").value & "</font></A></TD>"
                	Else
                		Response.Write "<TD Align=center noWrap width='8%'>" & rsAY.fields("ic_yld").value & "</TD>"
                	End If
                %>
                
                <!--<TD Align=center noWrap width='8%'><%=rsAY.fields("ic_yld").value%></TD>-->
                <TD Align=center noWrap width='0%'><%=rsAY.fields("yld_inv").value%></TD>
                <TD Align=center noWrap width='1%'><%=rsAY.fields("yld_act").value%></TD>
                <TD Align=center noWrap width='1%'><%=rsAY.fields("yld_asy").value%></TD>
                <TD Align=center noWrap width='1%'><%=rsAY.fields("ship_date").value%></TD>
                <TD Align=center noWrap width='1%'><%=rsAY.fields("ship_qty").value%></TD>
              </TR>
              <%
              	Dim lcType
              	lcType=rsAY.fields("pkg").value & " " & rsLC.fields("l_c").value & " " & rsAY.fields("pkg_size").value
              	rsAY.MoveNext
                Loop
              %>
              <TR bgcolor="#FFFF99">
              	<%
                If Request.Form("select_week")="" Then
                	sqlTotal =	"SELECT sum(dice_in)a,sum(dice_out)b,round(avg(dice_rate),2)c,sum(die_in)d,sum(die_out)e,round(avg(die_yld),2)f,"&_
                			"sum(pre_in)g,sum(pre_out)h,round(avg(pre_yld),2)i,sum(ic_in)j,sum(ic_out)k,round(avg(ic_yld),2)l,"&_
                			"round(avg(yld_inv),2)m,round(avg(yld_act),2)n,round(avg(yld_asy),2)o,sum(ship_qty)p "&_
							"FROM CS_MET_YIELD WHERE l_c='" & rsLC.fields("l_c").value & "' and week=to_char(sysdate,'ww')  group by l_c"
				Else
					sqlTotal =	"SELECT sum(dice_in)a,sum(dice_out)b,round(avg(dice_rate),2)c,sum(die_in)d,sum(die_out)e,round(avg(die_yld),2)f,"&_
                			"sum(pre_in)g,sum(pre_out)h,round(avg(pre_yld),2)i,sum(ic_in)j,sum(ic_out)k,round(avg(ic_yld),2)l,"&_
                			"round(avg(yld_inv),2)m,round(avg(yld_act),2)n,round(avg(yld_asy),2)o,sum(ship_qty)p "&_
							"FROM CS_MET_YIELD WHERE l_c='" & rsLC.fields("l_c").value & "' and week='" & Request.Form("select_week") & "' group by l_c"
				End If
				
				Set rsTotal=conn.Execute(sqlTotal)
				%> 
                <TD class=downloads Align=center noWrap width='7%'><b>Total</b></TD>
                <TD class=downloads Align=center noWrap width='7%' colspan="5"><%=lcType%></TD>
                <TD class=downloads Align=center noWrap width='2%'><%=rsTotal.fields("a").value%></TD>
                <TD class=downloads Align=center noWrap width='4%'><%=rsTotal.fields("b").value%></TD>
                <TD class=downloads Align=center noWrap width='9%'><%=rsTotal.fields("c").value%></TD>
                <TD class=downloads Align=center noWrap width='2%'><%=rsTotal.fields("d").value%></TD>
                <TD class=downloads Align=center noWrap width='4%'><%=rsTotal.fields("e").value%></TD>
                <TD class=downloads Align=center noWrap width='9%'><%=rsTotal.fields("f").value%></TD>
                <TD class=downloads Align=center noWrap width='2%'><%=rsTotal.fields("g").value%></TD>
                <TD class=downloads Align=center noWrap width='4%'><%=rsTotal.fields("h").value%></TD>
                <TD class=downloads Align=center noWrap width='8%'><%=rsTotal.fields("i").value%></TD>
                <TD class=downloads Align=center noWrap width='3%'><%=rsTotal.fields("j").value%></TD>
                <TD class=downloads Align=center noWrap width='4%'><%=rsTotal.fields("k").value%></TD>
                <TD class=downloads Align=center noWrap width='8%'><%=rsTotal.fields("l").value%></TD>
                <TD class=downloads Align=center noWrap width='0%'><%=rsTotal.fields("m").value%></TD>
                <TD class=downloads Align=center noWrap width='1%'><%=rsTotal.fields("n").value%></TD>
                <TD class=downloads Align=center noWrap width='1%'><%=rsTotal.fields("o").value%></TD>
                <TD class=downloads Align=center noWrap width='1%'>&nbsp;</TD>
                <TD class=downloads Align=center noWrap width='1%'><%=rsTotal.fields("p").value%></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </TD>
    <TD vAlign=top></TD>
  </TR>
</TABLE>

<!-- The End of Device Type -->

<%	
	rsLC.MoveNext
	Loop
%>
<%	
	Set rsAY = Nothing
	Set rsLC = Nothing
	Set rsWW = Nothing
	Set rsweek = Nothing
	Conn.Close
%>

<!--#include file="Excel_AY.inc" -->
<TABLE width="100%" border="0">                       
  <TR align="center" class=footer><TD>[ <A HREF='#report_top'>TOP</A> ]</TD></TR>
</TABLE>
</BODY>
</HTML>