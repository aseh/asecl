﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.IO;
using System.Net;
using System.Configuration;

namespace ASECL.Services
{
    public class EmailService
    {
        private string FromMail = "ct15504@dm.cloudmax.com.tw";
        private string FromName = "ASE Group";
        private string SmtpHost = "dm.cloudmax.com.tw";
        private string Username = "ct15504@dm.cloudmax.com.tw";
        private string Password = "Q3^@08D5";
        private int Port = 2525;

        public void Send(List<string> Mails, string Subject, string Body)
        {
            var MM = new MailMessage()
            {
                From = new MailAddress(FromMail, FromName),
                Subject = Subject,
                Body = Body,
                IsBodyHtml = true,
            };

            foreach (var Mail in Mails)
            {
                MM.To.Add(new MailAddress(Mail));
            }

            using (var Smtp = new SmtpClient(SmtpHost, Port)
            {
                Credentials = new NetworkCredential(Username, Password),
                EnableSsl = false,
            })
            {

                try
                {
                    Smtp.Send(MM);
                }
                catch (Exception err)
                {
                    Smtp.Dispose();
                }
            }
        }
    }
}