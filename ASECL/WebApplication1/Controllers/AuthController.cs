﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using ASECL.Models;

namespace ASECL.Controllers
{

    public class LoginBody
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class UpdatePassword
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
    }
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        
        [HttpPost]
        [Route("")]
        
        public IHttpActionResult Login([FromBody] LoginBody data)
        {
            string hasedPwd = Crypto.SHA256(data.password);
            HttpResponseMessage HRM = new HttpResponseMessage();
            try
            {
                Admin dbData = Db.Admin.First(x => x.Username == data.username && x.Password == hasedPwd);
                if ( dbData.IsDisabled == true )
                {
                    return Json(new ErrorResponse("該帳號已停用"));
                }
                generateToken(dbData);
                return Json(new ResultResponse<Admin>(dbData));
            } catch ( Exception err )
            {
                return Json(new ErrorResponse("登入失敗"));
            }
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Profile()
        {
            try
            {
                
                return Json(new ResultResponse<Admin>(validToken()));
            } catch ( Exception err )
            {
                return Json(new ErrorResponse("認證失敗", 401));
            }
        }

        [HttpDelete]
        [Route("")]
        public IHttpActionResult Logout()
        {
            try
            {
                Admin data = validToken();
                clearToken();
                return Json(new ResultResponse<Admin>(data));
            } catch ( Exception )
            {
                return Json(new ErrorResponse("認證失敗", 401));
            }
        }

        [HttpPut]
        [Route("password")]
        public IHttpActionResult UpdatePassword([FromBody] UpdatePassword data )
        {

            try
            {
                Admin currentUser = validToken();
                if (currentUser.Password != Crypto.SHA256(data.oldPassword))
                {
                    return Json(new ErrorResponse("舊密碼錯誤"));
                }

                if (data.newPassword != data.confirmPassword)
                {
                    return Json(new ErrorResponse("新密碼與確認密碼不一致"));
                }

                currentUser.Password = Crypto.SHA256(data.newPassword);
                Db.SaveChanges();

                return Json(new ResultResponse<Admin>(currentUser));


            } catch ( Exception )
            {
                return Json(new ErrorResponse("認證失敗", 401));
            }


        }
    }
}