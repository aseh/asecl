﻿using System.Web;
using System.Web.Http;
using ASECL.Models;
using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using System.Linq;

namespace ASECL.Controllers
{
    
    public class BaseApiController : ApiController
    {
        private string secret = "BOXKey";
        public Entities Db;
        public BaseApiController()
        {
            Db = new Entities();
        }
        [NSwag.Annotations.SwaggerIgnore]
        public void clearToken()
        {
            HttpContext.Current.Response.Cookies.Get("token").Value = "";
        }
        [NSwag.Annotations.SwaggerIgnore]
        public void generateToken(Admin data)
        {
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("token", encoder.Encode(data, secret)));
            //return encoder.Encode(data, JWTKey);
        }
        [NSwag.Annotations.SwaggerIgnore]
        public Admin validToken()
        {
            
            string token = HttpContext.Current.Request.Cookies.Get("token").Value;
            IJsonSerializer serializer = new JsonNetSerializer();
            var provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

            Admin json = decoder.DecodeToObject<Admin>(token, secret, verify: true);
            Admin dbData = Db.Admin.Where(x => x.Id == json.Id).FirstOrDefault();

            return dbData;
        }
    }
}