﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using NSwag.Annotations;
using ASECL.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.IO;
using ASECL.Services;

namespace ASECL.Controllers
{
   
    [RoutePrefix("api/certs")]
    public class CertController : BaseApiController
    {
        EmailService Email = new EmailService();


        private string GetCurrentUrl()
        {
            var Url = HttpContext.Current.Request.Url;
            string applicationPath = HttpContext.Current.Request.ApplicationPath == "/" ? "" : HttpContext.Current.Request.ApplicationPath;
            return string.Format("{0}://{1}{2}{3}", Url.Port != 443 ? "http" : "https", Url.Host, (Url.Port != 80 && Url.Port != 443) ? ":" + Url.Port : "", applicationPath);
        }

        private void SendEmail(bool isAdd, string Title, long Id)
        {
            var EmailList = Db.Email.Select(x => x.Address).ToList();
            var EmailTitle = (isAdd ? "新增 " : "更新 ") + Title + " 證書通知";
            Email.Send(EmailList, EmailTitle, String.Format("連結請參考： {0}/content/QualityCertification.html#{1}", GetCurrentUrl(), Id) );

        }


        private void SendEmailForDelete( string Title)
        {
            var EmailList = Db.Email.Select(x => x.Address).ToList();
            var EmailTitle = "刪除" + Title + " 證書通知";
            Email.Send(EmailList, EmailTitle, String.Format("{0} 證書已刪除，連結請參考： {1}/content/QualityCertification.html", Title, GetCurrentUrl()));

        }



        private Cert FC2Data()
        {
            Cert data = new Cert();
            var Form = HttpContext.Current.Request.Form;
            var Files = HttpContext.Current.Request.Files;
            data.Content = Form.Get("Content");
            data.Title = Form.Get("Title");
            data.IsDisabled = Form.Get("IsDisabled") == "true";
            data.CreatedAt = DateTime.Now;
            data.UpdatedAt = DateTime.Now;
            try
            {
                string sortNum = Form.Get("SortNum");
                if (sortNum != "" )
                {
                    data.SortNum = int.Parse(sortNum);
                }
                
            } catch ( Exception err )
            {
                data.SortNum = 0 ;
            }
            if (Files["PdfFile"] != null)
            {
                string PdfPath = string.Format("/Uploads/Pdfs/{0}.pdf", Guid.NewGuid().ToString());
                Files["PdfFile"].SaveAs(string.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, PdfPath));
                data.File = PdfPath;
            }
            if ( Files["ImageFile"] != null )
            {
                FileInfo fi = new FileInfo(Files["ImageFile"].FileName);
                string ImageFilePath = string.Format("/Uploads/Images/{0}{1}", Guid.NewGuid().ToString(), fi.Extension);
                Files["ImageFile"].SaveAs(string.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, ImageFilePath));
                data.Image = ImageFilePath;
            }

            return data;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult getList()
        {
            validToken();
            return Json(new ListResponse<Cert>(Db.Cert.OrderBy(x => x.SortNum).ThenBy(x => x.Id).ToList()));
        }

        [HttpPost]
        [Route("")]
        [OpenApiExtensionData("summary", "")]
        [Consumes("multipart/form-data")]
        public async Task<IHttpActionResult> Add()
        {
            validToken();
            try
            {
                
                Cert data = FC2Data();
                List<Cert> certList = Db.Cert.OrderBy(x => x.SortNum).ToList();
                try
                {
                    certList.Insert(data.SortNum - 1, data);
                }
                catch (Exception err)
                {
                    certList.Add(data);
                }
                
                int sortNum = 0;
                foreach ( var item in certList ) {
                    item.SortNum = ++sortNum;
                }

                data = Db.Cert.Add(data);
                Db.SaveChanges();
                SendEmail(true, data.Title, data.Id);
                return Json(new ResultResponse<Cert>(data));
            } catch ( Exception err )
            {
                return Json(new ErrorResponse("新增失敗"));
            }
            
        }

        /// <summary>
        /// 3213
        /// </summary>
        /// <param name="Id">2321</param>
        /// <param name="PdfFile">123</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{Id}")]
        [Consumes("multipart/form-data")]
        public async Task<IHttpActionResult> Update(int Id)
        {
            validToken();
            try
            {
                Cert data = FC2Data();
                try
                {
                    Cert dbData = Db.Cert.Where(x => x.Id == Id).First();

                    

                    dbData.Title = data.Title;
                    dbData.Content = data.Content;
                    dbData.IsDisabled = data.IsDisabled;
                    dbData.UpdatedAt = DateTime.Now;
                    if (data.Image != null)
                    {
                        dbData.Image = data.Image;
                    }
                    if (data.File != null)
                    {
                        dbData.File = data.File;
                    }


                    if (dbData.SortNum != data.SortNum)
                    {
                        List<Cert> certList = Db.Cert.Where(x => x.Id != Id).OrderBy(x => x.SortNum).ToList();
                        try
                        {
                            certList.Insert(data.SortNum - 1, dbData);
                        } catch ( Exception err )
                        {
                            certList.Add(dbData);
                        }
                        int sortNum = 0;
                        foreach( var item in certList)
                        {
                            item.SortNum = ++sortNum;
                        }
                    }


                    Db.SaveChanges();

                    SendEmail(false, data.Title, Id);

                    return Json(new ResultResponse<Cert>(dbData));
                }
                catch (Exception err)
                {
                    return Json(new ErrorResponse("查無資料"));
                };
            } catch (Exception err )
            {
                return Json(new ErrorResponse("編輯異常"));
            }
            
        }

        [HttpDelete]
        [Route("{Id}")]
        public IHttpActionResult DelItem(int Id)
        {
            validToken();
            try
            {
                Cert dbData = Db.Cert.Where(x => x.Id == Id).First();
                List<Cert> items = Db.Cert.Where(x => x.Id != Id).ToList();
                int sortNum = 0;
                foreach(var item in items )
                {
                    item.SortNum = ++sortNum;
                }
                Db.Cert.Remove(dbData);
                Db.SaveChanges();

                SendEmailForDelete(dbData.Title);

                return Json(new ResultResponse<Cert>(dbData));
            }
            catch (Exception err)
            {
                return Json(new ErrorResponse("查無資料"));
            }
        }
    }
}