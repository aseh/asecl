﻿using System;
using System.Linq;
using System.Web.Http;
using ASECL.Models;

namespace ASECL.Controllers
{
    [RoutePrefix("api/emails")]
    public class EmailController : BaseApiController
    {

        [Route("")]
        [HttpGet]
        public IHttpActionResult EmailList()
        {
            return Json(new ListResponse<Email>(Db.Email.ToList()));
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult AddEmail([FromBody] Email data)
        {
            validToken();

            if (Db.Email.Any(x => x.Address == data.Address))
            {
                return Json(new ErrorResponse("Email重覆"));
            }
            Db.Email.Add(new Email() { 
                Address = data.Address,
                Title = data.Title
            });
            Db.SaveChanges();
            return Json(new ResultResponse<Email>(data));
        }

        [Route("{Id}")]
        [HttpDelete]
        public IHttpActionResult DeleteEmail(int Id)
        {
            try
            {
                var DbData = Db.Email.Where(x => x.Id == Id).FirstOrDefault();
                Db.Email.Remove(DbData);
                Db.SaveChanges();
                return Json(new ResultResponse<Email>(DbData));
            } catch  (ArgumentNullException)
            {
                return Json(new ErrorResponse("查無資料"));
            }
        }
    }
}