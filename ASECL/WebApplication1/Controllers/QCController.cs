﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASECL.Models;

namespace ASECL.Controllers
{
    public class QCController : Controller
    {
        // GET: QC
        public ActionResult Index()
        {
            Entities Db = new Entities();
            List<Cert> Certs = Db.Cert.Where(x => x.IsDisabled == false).OrderBy(x => x.SortNum).ThenBy(x => x.Id).ToList();

            return View(Certs);
        }
    }
}